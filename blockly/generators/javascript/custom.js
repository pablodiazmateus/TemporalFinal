/*
---------------------------------------------------------
                    Programa e instrucciones
---------------------------------------------------------
*/


Blockly.JavaScript['programa'] = function (block) {
    var entrada_programa = Blockly.JavaScript.statementToCode(block, 'ENTRADAS');
    var code = 'programa ' + ' {\n' + entrada_programa + '\n}\n';
    return code;
};


Blockly.JavaScript['instrucciones'] = function (block) {
    var entrada_instrucciones = Blockly.JavaScript.statementToCode(block, 'PRINCIPAL');
    var code = 'instrucciones {\n' + entrada_instrucciones + '\n}\n';
    return code;
};


/*
---------------------------------------------------------
                   Declaración de Variables 
---------------------------------------------------------
*/


Blockly.JavaScript['dec_variable'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var entradas = Blockly.JavaScript.statementToCode(block, 'ENTRADA');
    if (entradas != "") {
        var code = 'var ' + nombre_var + ': ' + tipo + ' = ' + entradas + '\n';
    } else {
        var code = 'var ' + nombre_var + ': ' + tipo + '\n';
    }
    return code;
};


Blockly.JavaScript['dec_arreglo'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var tamaño = block.getFieldValue('TAMAÑO');
    var entradas = Blockly.JavaScript.statementToCode(block, 'ENTRADA');
    if (entradas != "") {
        var code = 'var ' + nombre_var + ': Arreglo<' + tipo + '>[' + tamaño + '] = [' + entradas + '] \n';
    } else {
        var code = 'var ' + nombre_var + ': Arreglo<' + tipo + '>[' + tamaño + '] \n';
    }
    code = code.replace(/[()]/g, '');
    code = code.replace(/[¿]/g, '(');
    code = code.replace(/[?]/g, ')');
    return code;
};


/*
---------------------------------------------------------
                   Asignación de Variables 
---------------------------------------------------------
*/


Blockly.JavaScript['asig_variable'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var valor = block.getFieldValue('VALOR');
    var code = 'var ' + nombre_var + ': ' + tipo + ' = ' + valor + '\n';
    return code;
};


Blockly.JavaScript['asig_arreglo'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var tamaño = block.getFieldValue('TAMAÑO');
    var valor = block.getFieldValue('VALOR');
    var code = 'var ' + nombre_var + ': Arreglo<' + tipo + '>[' + tamaño + '] = [' + valor + '] \n';
    code = code.replace(/[()]/g, '');
    code = code.replace(/[¿]/g, '(');
    code = code.replace(/[?]/g, ')');
    return code;
};


/*
---------------------------------------------------------
                    Usar Variables 
---------------------------------------------------------
*/


Blockly.JavaScript['uso_asignacion'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var entrada = Blockly.JavaScript.statementToCode(block, 'ENTRADA');
    var code = nombre_var + ' = ' + entrada + '\n';
    code = code.replace(/[()]/g, '');
    code = code.replace(/[¿]/g, '(');
    code = code.replace(/[?]/g, ')');
    return code;
};


Blockly.JavaScript['variable'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var code = nombre_var;
    return code;
};


Blockly.JavaScript['variables'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var valor = Blockly.JavaScript.statementToCode(block, 'VALOR');
    var code = nombre_var + ', ' + valor;
    return code;
};


/*
---------------------------------------------------------
                   Constantes 
---------------------------------------------------------
*/


Blockly.JavaScript['constante_booleana'] = function (block) {
    var valor = block.getFieldValue('VALOR');
    var code = valor;
    return code;
};


Blockly.JavaScript['constante_texto'] = function (block) {
    var valor = block.getFieldValue('VALOR');
    var code = '"' + valor + '"';
    return code;
};


Blockly.JavaScript['constante_flotante'] = function (block) {
    var valor = block.getFieldValue('VALOR');
    var code = valor;
    return code;
};


Blockly.JavaScript['constante_entera'] = function (block) {
    var valor = block.getFieldValue('VALOR');
    var code = valor;
    return code;
};


Blockly.JavaScript['constante_arreglo'] = function (block) {
    var valor = block.getFieldValue('VALOR');
    var code = '[' + valor + ']';
    return code;
};


Blockly.JavaScript['constante_numerica_coma'] = function (block) {
    var entrada = Blockly.JavaScript.statementToCode(block, 'ENTRADA');
    var valor = block.getFieldValue('VALOR');
    var code = valor + ' , ' + entrada;
    return code;
};


/*
---------------------------------------------------------
                    Funciones
---------------------------------------------------------
*/


Blockly.JavaScript['funcion_param'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var parametros = Blockly.JavaScript.valueToCode(block, 'PARAMETROS', Blockly.JavaScript.ORDER_ATOMIC);
    var cuerpo_funcion = Blockly.JavaScript.statementToCode(block, 'FUNCION');
    parametros = parametros.replace(/[()]/g, '');
    var code = 'funcion ' + nombre_var + '(' + parametros + ') ->' + tipo + '{\n' + cuerpo_funcion + '\n}\n';
    return code;
};


Blockly.JavaScript['funcion_sin_parametros'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var cuerpo_funcion = Blockly.JavaScript.statementToCode(block, 'FUNCION');
    var code = 'funcion ' + nombre_var + '() ->' + tipo + '{\n' + cuerpo_funcion + '\n}\n';
    return code;
};


Blockly.JavaScript['parametro_final'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var code = nombre_var + ': ' + tipo;
    return [code, Blockly.JavaScript.ORDER_NONE];
};


Blockly.JavaScript['mas_parametros'] = function (block) {
    var tipo = block.getFieldValue('TIPO');
    var nombre_var = block.getFieldValue('NOMBRE');
    var parametros = Blockly.JavaScript.valueToCode(block, 'PARAMETROS', Blockly.JavaScript.ORDER_ATOMIC);
    var code = nombre_var + ': ' + tipo + ', ' + parametros;
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};


Blockly.JavaScript['llamar_funcion_Par'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var parametros = Blockly.JavaScript.statementToCode(block, 'PARAMETROS');
    var code = nombre_var + '¿ ' + parametros + ' ?\n';
    code = code.replace(/[()]/g, '');
    code = code.replace(/[¿]/g, '(');
    code = code.replace(/[?]/g, ')');
    return code;
};


Blockly.JavaScript['asignar_funcion_Par'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var parametros = Blockly.JavaScript.statementToCode(block, 'PARAMETROS');
    var code = nombre_var + '¿ ' + parametros + ' ?\n';
    code = code.replace(/[()]/g, '');
    code = code.replace(/[¿]/g, '(');
    code = code.replace(/[?]/g, ')');
    return code;
};


Blockly.JavaScript['llamar_funcion_No_Par'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var code = nombre_var + '()';
    return code;
};


Blockly.JavaScript['asignar_funcion_No_Par'] = function (block) {
    var nombre_var = block.getFieldValue('NOMBRE');
    var code = nombre_var + '()';
    return code;
};


Blockly.JavaScript['regresa'] = function (block) {
    var regresa = Blockly.JavaScript.statementToCode(block, 'REGRESA');
    var code = 'regresa' + regresa + '\n';
    return code;
};


Blockly.JavaScript['nada'] = function (block) {
    var code = 'Nada';
    return code;
};


/*
---------------------------------------------------------
                   Operaciones Lógicas
---------------------------------------------------------
*/


Blockly.JavaScript['logicas_si_entonces'] = function (block) {
    var condicion = Blockly.JavaScript.statementToCode(block, 'CONDICION');
    var accion = Blockly.JavaScript.statementToCode(block, 'ACCION');
    var code = 'si ' + condicion + '{\n' + accion + '}\n';
    return code;
};


Blockly.JavaScript['logicas_si_no'] = function (block) {
    var accion = Blockly.JavaScript.statementToCode(block, 'ACCION');
    var code = 'sino' + '{ \n' + accion + '}\n';
    return code;
};

Blockly.JavaScript['logicas_compara'] = function (block) {
    var arg1 = Blockly.JavaScript.statementToCode(block, 'ENTRADA1');
    var arg2 = Blockly.JavaScript.statementToCode(block, 'ENTRADA2');
    var tipo = block.getFieldValue('TIPO');
    var code = '(' + arg1 + ' ' + tipo + arg2 + ')\n';
    return code;
};

Blockly.JavaScript['logicas_operacion'] = function (block) {
    var arg1 = Blockly.JavaScript.statementToCode(block, 'ENTRADA1');
    var arg2 = Blockly.JavaScript.statementToCode(block, 'ENTRADA2');
    var tipo = block.getFieldValue('TIPO');
    var code = '(' + arg1 + ' ' + tipo + arg2 + ')\n';
    return code;
};

Blockly.JavaScript['logicas_negar'] = function (block) {
    var arg = Blockly.JavaScript.statementToCode(block, 'ENTRADA');
    var code = '!' + arg;
    return code;
};


/*
---------------------------------------------------------
                   Aritméticas
---------------------------------------------------------
*/


Blockly.JavaScript['aritmetica'] = function (block) {
    var arg1 = Blockly.JavaScript.statementToCode(block, 'ENTRADA1');
    var arg2 = Blockly.JavaScript.statementToCode(block, 'ENTRADA2');
    var tipo = block.getFieldValue('TIPO');
    var code = '(' + arg1 + ' ' + tipo + arg2 + ')';
    return code;
};


/*
---------------------------------------------------------
                   Ciclos
---------------------------------------------------------
*/


Blockly.JavaScript['mientras_entonces'] = function (block) {
    var condicion = Blockly.JavaScript.statementToCode(block, 'CONDICION');
    var accion = Blockly.JavaScript.statementToCode(block, 'ACCION');
    var code = 'mientras ' + condicion + ' { \n' + accion + '}\n';
    return code;
};


/*
---------------------------------------------------------
                        Leer/Imprimir 
---------------------------------------------------------
*/


Blockly.JavaScript['leer'] = function (block) {
    var code = ' lee ()';
    return code;
};


Blockly.JavaScript['imprimir'] = function (block) {
    var valor = Blockly.JavaScript.statementToCode(block, 'VALOR');
    var code = 'imprime(' + valor + ')\n';
    return code;
};
