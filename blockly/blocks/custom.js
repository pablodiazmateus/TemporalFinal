'use strict';

goog.provide('Blockly.Blocks.custom');

goog.require('Blockly.Blocks');


/*
---------------------------------------------------------
                    Programa e instrucciones 
---------------------------------------------------------
*/


Blockly.Blocks['programa'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Programa")
        this.appendStatementInput("ENTRADAS")
            .setCheck(null);
        this.setColour("#E8250C");
        this.setTooltip('Aquí se declaran las funciones, variables globales y el set de instrucciones');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['instrucciones'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Instrucciones");
        this.appendStatementInput("PRINCIPAL")
            .setCheck(null);
        this.setPreviousStatement(true, null);
        this.setColour("#121EFF");
        this.setTooltip('Aquí se declara el set de instrucciones a programar');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                   Declaración de Variables 
---------------------------------------------------------
*/


Blockly.Blocks['dec_variable'] = {
    init: function () {
        this.appendValueInput("ENTRADA")
            .setCheck(['Texto', 'Entero', 'Flotante', 'Booleano'])
            .appendField("var ")
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE")
            .appendField(" : tipo")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#000000");
        this.setTooltip('El nombre debe ir junto siempre.');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['dec_arreglo'] = {
    init: function () {
        this.appendValueInput("ENTRADA")
            .setCheck(null)
            .appendField("var ")
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE")
            .appendField(" : tipo ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO")
            .appendField("[")
            .appendField(new Blockly.FieldNumber(0, 1), "TAMAÑO")
            .appendField("]")
            .appendField("=");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#000000");
        this.setTooltip('El nombre debe ir junto siempre.');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                   Asignación de Variables 
---------------------------------------------------------
*/


Blockly.Blocks['asig_variable'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("var ")
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE")
            .appendField(" : tipo")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO")
            .appendField("=")
            .appendField(new Blockly.FieldTextInput("valor"), "VALOR");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#000000");
        this.setTooltip('El nombre debe ir junto siempre.');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['asig_arreglo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("var ")
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE")
            .appendField(" : tipo ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO")
            .appendField("[")
            .appendField(new Blockly.FieldNumber(0, 1), "TAMAÑO")
            .appendField("]")
            .appendField("= [")
            .appendField(new Blockly.FieldTextInput("val1, val2"), "VALOR")
            .appendField("]");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#000000");
        this.setTooltip('Los valores deben ir separados por , (coma) y entre corchetes [ ]');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                    Usar Variables 
---------------------------------------------------------
*/


Blockly.Blocks['uso_asignacion'] = {
    init: function () {
        this.appendValueInput("ENTRADA")
            .setCheck(['Texto', 'Entero', 'Flotante', 'Booleano', 'Number', "Multiple", ""])
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE")
            .appendField("=");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#000000");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['variable'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE");
        this.setOutput(true, null);
        this.setColour("#000000");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['variables'] = {
    init: function () {
        this.appendValueInput("VALOR")
            .setCheck(null)
            .appendField(new Blockly.FieldTextInput("nombreVariable"), "NOMBRE");
        this.setOutput(true, null);
        this.setColour("#000000");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                    Constantes
---------------------------------------------------------
*/


Blockly.Blocks['constante_booleana'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["Verdadero", "Verdadero"], ["Falso", "Falso"]]), "VALOR")
        this.setOutput(true, 'Booleano')
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['constante_texto'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("Texto"), "VALOR");
        this.setOutput(true, 'Texto')
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['constante_flotante'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldNumber("Flotante"), "VALOR");
        this.setOutput(true, 'Flotante')
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['constante_entera'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldNumber("Entero"), "VALOR");
        this.setOutput(true, 'Entero')
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['constante_arreglo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("[")
            .appendField(new Blockly.FieldTextInput("val1, val2, 'texto1', 'texto2'"), "VALOR")
            .appendField("]");
        this.setOutput(true, null)
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['constante_numerica_coma'] = {
    init: function () {
        this.appendValueInput("ENTRADA")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple'])
            .appendField(new Blockly.FieldNumber(0), "VALOR")
            .appendField(",");
        this.setOutput(true, 'Multiple');
        this.setColour("#FFDE00");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                   Funciones 
---------------------------------------------------------
*/


Blockly.Blocks['funcion_param'] = {
    init: function () {
        this.appendValueInput("PARAMETROS")
            .setCheck('Parametros')
            .appendField("funcion")
            .appendField(new Blockly.FieldTextInput("nombreFuncion"), "NOMBRE")
            .appendField(" retorno: ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Flotante", "Flotante"], ["Texto", "Texto"], ["SinRetorno", "Nada"]]), "TIPO");
        this.appendStatementInput("FUNCION")
            .setCheck(null)
            .appendField("Regresa: ");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['funcion_sin_parametros'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("funcion")
            .appendField(new Blockly.FieldTextInput("nombreFuncion"), "NOMBRE")
            .appendField(" retorno: ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Flotante", "Flotante"], ["Texto", "Texto"], ["SinRetorno", "Nada"]]), "TIPO");
        this.appendStatementInput("FUNCION")
            .setCheck(null)
            .appendField("Regresa: ");
        this.setPreviousStatement(true, 'Parametros');
        this.setNextStatement(true, 'Parametros');
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['parametro_final'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Tipo de dato: ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO")
            .appendField(new Blockly.FieldTextInput("nombreParámetro"), "NOMBRE");
        this.setOutput(true, 'Parametros');
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['mas_parametros'] = {
    init: function () {
        this.appendValueInput("PARAMETROS")
            .setCheck(null)
            .appendField("Tipo de dato: ")
            .appendField(new Blockly.FieldDropdown([["Booleano", "Booleano"], ["Entero", "Entero"], ["Texto", "Texto"], ["Flotante", "Flotante"]]), "TIPO")
            .appendField(new Blockly.FieldTextInput("nombreParámetro"), "NOMBRE");
        this.setOutput(true, 'Parametros');
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['llamar_funcion_Par'] = {
    init: function () {
        this.appendValueInput("PARAMETROS")
            .setCheck(null)
            .appendField(new Blockly.FieldTextInput("nombreFunción"), "NOMBRE");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['asignar_funcion_Par'] = {
    init: function () {
        this.appendValueInput("PARAMETROS")
            .setCheck(null)
            .appendField(new Blockly.FieldTextInput("nombreFunción"), "NOMBRE");
        this.setOutput(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['llamar_funcion_No_Par'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("nombreFunción"), "NOMBRE");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['asignar_funcion_No_Par'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("nombreFunción"), "NOMBRE");
        this.setOutput(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['regresa'] = {
    init: function () {
        this.appendValueInput("REGRESA")
            .setCheck(null)
            .appendField("Regresa: ");
        this.setPreviousStatement(true, null);
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['nada'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput("Nada"), "NADA");
        this.setOutput(true, 'Return');
        this.setColour("#3A9C2D");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                   Operaciones Lógicas
---------------------------------------------------------
*/


Blockly.Blocks['logicas_si_entonces'] = {
    init: function () {
        this.appendValueInput("CONDICION")
            .setCheck(null)
            .appendField("Si");
        this.appendStatementInput("ACCION")
            .setCheck(null)
            .appendField("Entonces ");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }

};


Blockly.Blocks['logicas_si_no'] = {
    init: function () {
        this.appendStatementInput("ACCION")
            .setCheck(null)
            .appendField("Si no");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('');
    }
};

Blockly.Blocks['logicas_compara'] = {
    init: function () {
        this.appendValueInput("ENTRADA1")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["==", "=="], ["!=", "!="], [">", ">"], ["<", "<"], [">=", ">="], ["<=", "<="]]), "TIPO");
        this.appendValueInput("ENTRADA2")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput();
        this.setOutput(true, 'Multiple');
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['logicas_operacion'] = {
    init: function () {
        this.appendValueInput("ENTRADA1")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["Y", "&&"], ["O", "||"]]), "TIPO");
        this.appendValueInput("ENTRADA2")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput();
        this.setOutput(true, 'Multiple');
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['logicas_negar'] = {
    init: function () {
        this.appendValueInput("ENTRADA")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple'])
            .appendField("Negar");
        this.setOutput(true, 'Multiple');
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

/*
---------------------------------------------------------
                   Operaciones Numéricas
---------------------------------------------------------
*/


Blockly.Blocks['aritmetica'] = {
    init: function () {
        this.appendValueInput("ENTRADA1")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["+", "+"], ["-", "-"], ["*", "*"], ["/", "/"], ["%", "%"]]), "TIPO");
        this.appendValueInput("ENTRADA2")
            .setCheck(['Flotante', 'Entero', 'Texto', 'Booleano', 'Multiple']);
        this.appendDummyInput();
        this.setOutput(true, 'Multiple');
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                   Ciclos
---------------------------------------------------------
*/


Blockly.Blocks['mientras_entonces'] = {
    init: function () {
        this.appendValueInput("CONDICION")
            .setCheck(null)
            .appendField("Mientras");
        this.appendStatementInput("ACCION")
            .setCheck(null)
            .appendField("Entonces ");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#E183E8");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


/*
---------------------------------------------------------
                        Leer/Imprimir 
---------------------------------------------------------
*/


Blockly.Blocks['leer'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Leer ()");
        this.setOutput(true, '');
        this.setColour("#6BE8F5");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


Blockly.Blocks['imprimir'] = {
    init: function () {
        this.appendValueInput("VALOR")
            .setCheck(null)
            .appendField("Imprimir");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour("#6BE8F5");
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};
