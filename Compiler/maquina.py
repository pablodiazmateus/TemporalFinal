from memoria import *
from cuboSemantico import *
# Import system module.
import sys

def executionError(error):
    sys.exit(error)

def executeCode(functions, quadruples, constants, varCounter):
    memory = Memoria(varCounter['globalCounter'], varCounter['localCounter'], varCounter['temporalCounter'], varCounter['pointersCounter'])
    instructionPointer = 0
    quadrupleStack = []
    quadruplesCount = len(quadruples)
    returnAddress = None
    while instructionPointer < quadruplesCount:
        quadruple = quadruples[instructionPointer]
        # SUMA DONE
        if quadruple['operation'] == operationCodes['+']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            if (instructionPointer - 1) >= 0 and quadruples[instructionPointer - 1]['operation'] == operationCodes['VERIFY'] :
                operand2Value = memory.getValue(operand2, constants)
                resultValue = operand1 + operand2Value
                memory.saveValue(result, resultValue, True)
            elif quadruples[instructionPointer - 1]['operation'] == operationCodes['VERIFYCONST']:
                resultValue = operand1 + operand2
                memory.saveValue(result, resultValue, True)
            else:
                operand1Value = memory.getValue(operand1, constants)
                operand2Value = memory.getValue(operand2, constants)
                operand1Scope = memory.getScope(operand1)
                operand2Scope = memory.getScope(operand2)
                operand1VarType = memory.getVarType(operand1, operand1Scope)
                operand2VarType = memory.getVarType(operand2, operand2Scope)
                if operand1Value == None or operand2Value == None:
                    executionError('ERROR: No hay valor en memoria')
                elif operand1VarType == TEXTO or operand2VarType == TEXTO:
                    resultValue = '{}{}'.format(operand1Value, operand2Value)
                    memory.saveValue(result, resultValue)
                else:
                    resultValue = operand1Value + operand2Value
                    memory.saveValue(result, resultValue)
        # RESTA DONE
        elif quadruple['operation'] == operationCodes['-']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value - operand2Value
                memory.saveValue(result, resultValue)
        # MULTIPLICACION DONE
        elif quadruple['operation'] == operationCodes['*']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value * operand2Value
                memory.saveValue(result, resultValue)
        # DIVISION DONE
        elif quadruple['operation'] == operationCodes['/']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            elif operand2Value == 0:
                executionError('ERROR: División entre 0')
            else:
                operand1Scope = memory.getScope(operand1)
                operand2Scope = memory.getScope(operand2)
                operand1VarType = memory.getVarType(operand1, operand1Scope)
                operand2VarType = memory.getVarType(operand2, operand2Scope)
                if operand1VarType == ENTERO and operand2VarType == ENTERO:
                    resultValue = round(operand1Value / operand2Value)
                    memory.saveValue(result, resultValue)
                else:
                    resultValue = operand1Value / operand2Value
                    memory.saveValue(result, resultValue)
        # MODULO DONE
        elif quadruple['operation'] == operationCodes['%']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            elif operand2Value == 0:
                executionError('ERROR: División entre 0')
            else:
                resultValue = operand1Value % operand2Value
                memory.saveValue(result, resultValue)
        # ASIGN DONE
        elif quadruple['operation'] == operationCodes['=']:
            operand1 = quadruple['variable1']
            result = quadruple['result']
            if type(operand1) is dict and type(result) is dict:
                list = []
                for i in range(operand1['size']):
                    address = result['address'] + i
                    value = memory.getValue(operand1['address'] + i, constants)
                    memory.saveValue(address, value)
            else:
                resultValue = memory.getValue(operand1, constants)
                if resultValue == None:
                    executionError('ERROR: No hay valor en memoria')
                else:
                    memory.saveValue(result, resultValue)

        # AND DONE
        elif quadruple['operation'] == operationCodes['&&']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value and operand2Value
                memory.saveValue(result, resultValue)

        # OR DONE
        elif quadruple['operation'] == operationCodes['||']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value or operand2Value
                memory.saveValue(result, resultValue)

        # NOT DONE
        elif quadruple['operation'] == operationCodes['!']:
            operand1 = quadruple['variable1']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            if operand1Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = not operand1Value
                memory.saveValue(result, resultValue)
        # MAYOR O IGUAL DONE
        elif quadruple['operation'] == operationCodes['>=']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value >= operand2Value
                memory.saveValue(result, resultValue)
        # MAYOR DONE
        elif quadruple['operation'] == operationCodes['>']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value > operand2Value
                memory.saveValue(result, resultValue)
        # MENOR O IGUAL DONE
        elif quadruple['operation'] == operationCodes['<=']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value <= operand2Value
                memory.saveValue(result, resultValue)

        # MENOR DONE
        elif quadruple['operation'] == operationCodes['<']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value < operand2Value
                memory.saveValue(result, resultValue)

        # EQUALS DONE
        elif quadruple['operation'] == operationCodes['==']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value == operand2Value
                memory.saveValue(result, resultValue)

        #DIFFERENT DONE
        elif quadruple['operation'] == operationCodes['!=']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            operand2Value = memory.getValue(operand2, constants)
            if operand1Value == None or operand2Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                resultValue = operand1Value != operand2Value
                memory.saveValue(result, resultValue)

        #PRINT DONE
        elif quadruple['operation'] == operationCodes['PRINT']:
            operand1 = quadruple['variable1']
            if type(operand1) is dict:
                list = []
                for i in range(operand1['size']):
                    value = memory.getValue(operand1['address'] + i, constants)
                    if value != None:
                        list.append(value)
                print('{}'.format(list))
            else:
                operand1Value = memory.getValue(operand1, constants)
                if operand1Value == None:
                    executionError('ERROR: No hay valor en memoria')
                else:
                    if type(operand1Value) == type(True):
                        operand1Value = 'verdadero' if operand1Value == True else 'falso'
                        print('{}'.format(operand1Value))
                    else:
                        print('{}'.format(operand1Value))

        #GOTO DONE
        elif quadruple['operation'] == operationCodes['GOTO']:
            instructionPointer = quadruple['result'] - 1

        elif quadruple['operation'] == operationCodes['GOTOF']:
            operand1 = quadruple['variable1']
            operand1Value = memory.getValue(operand1, constants)
            if not operand1Value:
                instructionPointer = quadruple['result'] - 1

        elif quadruple['operation'] == operationCodes['ENDPROC']:
            memory.recoverMemoryState()
            instructionPointer = quadrupleStack.pop() - 1
        elif quadruple['operation'] == operationCodes['ERA']:
            functionName = quadruple['variable1']
            memory.functionMemory(functions[functionName]['localVarCounter'], functions[functionName]['temporalCounter'])
        elif quadruple['operation'] == operationCodes['PARAMETER']:
            operand1 = quadruple['variable1']
            result = quadruple['result']
            operand1Value = memory.getValue(operand1, constants)
            if operand1Value == None:
                executionError('ERROR: No hay valor en memoria')
            else:
                memory.storeParam(result, operand1Value)

        elif quadruple['operation'] == operationCodes['GOTOSUB']:
            memory.saveMemoryState()
            quadrupleStack.append(instructionPointer + 1)
            if functions[quadruple['variable1']]['type'] != None:
                returnAddress = functions[quadruple['variable1']]['returnAddress']
            instructionPointer = functions[quadruple['variable1']]['counter']-1

        elif quadruple['operation'] == operationCodes['RETURN']:
            operand1 = quadruple['variable1']
            operand1Value = memory.getValue(operand1, constants)
            memory.saveValue(returnAddress, operand1Value)

        # READ DONE
        elif quadruple['operation'] == operationCodes['READ']:
            result = quadruple['result']
            resultScope = memory.getScope(result)
            resultVarType = memory.getVarType(result, resultScope)
            if resultVarType == ENTERO:
                try:
                    integer = int(input())
                    memory.saveValue(result, integer)
                except ValueError:
                    executionError('El valor ingresado no es Entero')
            elif resultVarType == FLOTANTE:
                try:
                    flotante = float(input())
                    memory.saveValue(result, flotante)
                except ValueError:
                    executionError('El valor ingresado no es Flotante')
            elif resultVarType == BOOLEANO:
                bool = input()
                boolValue = True
                if bool == 'verdadero':
                    boolValue = True
                    memory.saveValue(result, boolValue)
                elif bool == 'falso':
                    boolValue = False
                    memory.saveValue(result, boolValue)
                else:
                    executionError('El valor ingresado no es Booleano')
            elif resultVarType == TEXTO:
                text = input()
                memory.saveValue(result, text)

        elif quadruple['operation'] == operationCodes['VERIFY']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            operand2Value = memory.getValue(operand2, constants)
            if operand2Value > operand1:
                executionError('ERROR: Indice de arreglo mayor al tamaño del arreglo')

        elif quadruple['operation'] == operationCodes['VERIFYCONST']:
            operand1 = quadruple['variable1']
            operand2 = quadruple['variable2']
            if operand2 > operand1:
                executionError('ERROR: Indice de arreglo mayor al tamaño del arreglo')

        instructionPointer += 1
