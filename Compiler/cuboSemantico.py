################################################################################
#                                                                              #
# Type and operation codes                                                     #
#                                                                              #
################################################################################
#Type codes.
ENTERO          = 1
FLOTANTE        = 2
BOOLEANO        = 3
TEXTO           = 4
ARREGLOENTERO   = 5
ARREGLOFLOTANTE = 6
ARREGLOBOOLEANO = 7
ARREGLOTEXTO    = 8
operationCodes = {
# Operation codes.
# Math Operators.
'+' : 9, '-' : 10, '*' : 11, '/' : 12, '%' : 13, '=' : 14,
# Logical Operators.
'&&' : 15, '||' : 16, '!' : 17,
# Relational Operators
'>=' : 18, '>' : 19, '<=' : 20, '<' : 21, '==' : 22, '!=' : 23,
# OTHER CODES
'PRINT' : 24, 'GOTO' : 25, 'GOTOF' : 26, 'ERA' : 27, 'PARAMETER' : 28, 'GOTOSUB' : 29, 'ENDPROC' : 30, 'RETURN' : 31, 'READ' : 32, 'VERIFY' : 33, 'VERIFYCONST' : 34
}

################################################################################
#                                                                              #
# Semantic Dictionary                                                          #
#                                                                              #
################################################################################
resultTypes = {}
#Asignation Result Types.
resultTypes["Entero=Entero"]                       = ENTERO
resultTypes["Flotante=Flotante"]                   = FLOTANTE
resultTypes["Flotante=Entero"]                     = FLOTANTE
resultTypes["Booleano=Booleano"]                   = BOOLEANO
resultTypes["Texto=Texto"]                         = TEXTO
resultTypes["Arreglo<Entero>=Arreglo<Entero>"]     = ARREGLOENTERO
resultTypes["Arreglo<Flotante>=Arreglo<Flotante>"] = ARREGLOFLOTANTE
resultTypes["Arreglo<Booleano>=Arreglo<Booleano>"] = ARREGLOBOOLEANO
resultTypes["Arreglo<Texto>=Arreglo<Texto>"]       = ARREGLOTEXTO
# Addition Result Types.
resultTypes["Entero+Entero"]     = ENTERO
resultTypes["Flotante+Flotante"] = FLOTANTE
resultTypes["Flotante+Entero"]   = FLOTANTE
resultTypes["Entero+Flotante"]   = FLOTANTE
resultTypes["Texto+Texto"]       = TEXTO
resultTypes["Texto+Entero"]      = TEXTO
resultTypes["Texto+Flotante"]    = TEXTO
resultTypes["Texto+Booleano"]    = TEXTO
resultTypes["Entero+Texto"]      = TEXTO
resultTypes["Flotante+Texto"]    = TEXTO
resultTypes["Booleano+Texto"]    = TEXTO
# Substraction Result Types.
resultTypes["Entero-Entero"]     = ENTERO
resultTypes["Flotante-Flotante"] = FLOTANTE
resultTypes["Flotante-Entero"]   = FLOTANTE
resultTypes["Entero-Flotante"]   = FLOTANTE
# Product Result Types.
resultTypes["Entero*Entero"]     = ENTERO
resultTypes["Flotante*Flotante"] = FLOTANTE
resultTypes["Flotante*Entero"]   = FLOTANTE
resultTypes["Entero*Flotante"]   = FLOTANTE
# Division Result Types.
resultTypes["Entero/Entero"]     = ENTERO
resultTypes["Flotante/Flotante"] = FLOTANTE
resultTypes["Flotante/Entero"]   = FLOTANTE
resultTypes["Entero/Flotante"]   = FLOTANTE
# Modulo Result Types.
resultTypes["Entero%Entero"] = ENTERO
# Greater or equal than.
resultTypes["Entero>=Entero"]     = BOOLEANO
resultTypes["Flotante>=Flotante"] = BOOLEANO
resultTypes["Flotante>=Entero"]   = BOOLEANO
resultTypes["Entero>=Flotante"]   = BOOLEANO
# Greater than.
resultTypes["Entero>Entero"]     = BOOLEANO
resultTypes["Flotante>Flotante"] = BOOLEANO
resultTypes["Flotante>Entero"]   = BOOLEANO
resultTypes["Entero>Flotante"]   = BOOLEANO
# Less or equal than.
resultTypes["Entero<=Entero"]     = BOOLEANO
resultTypes["Flotante<=Flotante"] = BOOLEANO
resultTypes["Flotante<=Entero"]   = BOOLEANO
resultTypes["Entero<=Flotante"]   = BOOLEANO
# Less than.
resultTypes["Entero<Entero"]     = BOOLEANO
resultTypes["Flotante<Flotante"] = BOOLEANO
resultTypes["Flotante<Entero"]   = BOOLEANO
resultTypes["Entero<Flotante"]   = BOOLEANO
# Equals.
resultTypes["Entero==Entero"]     = BOOLEANO
resultTypes["Flotante==Flotante"] = BOOLEANO
resultTypes["Texto==Texto"]       = BOOLEANO
resultTypes["Booleano==Booleano"] = BOOLEANO
# Different.
resultTypes["Entero!=Entero"]     = BOOLEANO
resultTypes["Flotante!=Flotante"] = BOOLEANO
resultTypes["Texto!=Texto"]       = BOOLEANO
resultTypes["Booleano!=Booleano"] = BOOLEANO
#Logical Operators
resultTypes["Booleano&&Booleano"] = BOOLEANO
resultTypes["Booleano||Booleano"] = BOOLEANO

################################################################################
#                                                                              #
# Methods                                                                      #
#                                                                              #
################################################################################
# Returns Type Code.
def getTypeName(typeCode):
    if typeCode == ENTERO:
        return "Entero"
    elif typeCode == FLOTANTE:
        return "Flotante"
    elif typeCode == BOOLEANO:
        return "Booleano"
    elif typeCode == TEXTO:
        return "Texto"
    elif typeCode == ARREGLOENTERO:
        return "Arreglo<Entero>"
    elif typeCode == ARREGLOFLOTANTE:
        return "Arreglo<Flotante>"
    elif typeCode == ARRBOOLEANO:
        return "Arreglo<Booleano>"
    elif typeCode == ARREGLOTEXTO:
        return "Arreglo<Texto>"

# Returns Type Name.
def getTypeCode(typeName):
    if typeName == "Entero":
        return ENTERO
    elif typeName == "Flotante":
        return FLOTANTE
    elif typeName == "Booleano":
        return BOOLEANO
    elif typeName == "Texto":
        return TEXTO
    elif typeName == "Arreglo<Entero>":
        return ARREGLOENTERO
    elif typeName == "Arreglo<Flotante>":
        return ARREGLOFLOTANTE
    elif typeName == "Arreglo<Booleano>":
        return ARREGLOBOOLEANO
    elif typeName == "Arreglo<Texto>":
        return ARREGLOTEXTO

# Given 2 operands and an operator returns result Type Code.
def getResultType(operandType1, operator, operandType2):
    operandTypeName1 = getTypeName(operandType1)
    operandTypeName2 = getTypeName(operandType2)
    key = operandTypeName1 + operator + operandTypeName2
    if not key in resultTypes:
        return -1
    else:
        return resultTypes[key]
