# Import PLY's Lex and Yacc.
import ply.lex as lex
import ply.yacc as yacc

# Import Semantic Cube, memory and virtual machine.
from cuboSemantico import *
from memoria import *
from maquina import *

# Import system and json module.
import sys
from ast import literal_eval

################################################################################
#                                                                              #
# Scanner Section.                                                             #
#                                                                              #
################################################################################

# List of reserved words used in the language.
reserved = {
   'Booleano'      : 'BOOLEANO',      'Entero'    : 'ENTERO',
   'Flotante'      : 'FLOTANTE',      'Texto'     : 'TEXTO',
   'var'           : 'VAR',           'imprime'   : 'IMPRIME',
   'lee'           : 'LEE',           'funcion'   : 'FUNCION',
   'instrucciones' : 'INSTRUCCIONES', 'programa'  : 'PROGRAMA',
   'Nada'          : 'NADA',          'si'        : 'SI',
   'sino'          : 'SINO',          'mientras'  : 'MIENTRAS',
   'regresa'       : 'REGRESA',       'verdadero' : 'VERDADERO',
   'falso'         : 'FALSO',         'Arreglo'   : 'ARREGLO',
   'cuenta'        : 'CUENTA',
}

# List of Tokens.
tokens = [
'MAS','MENOS','MULTIPLICACION','DIVISION', 'MODULO', # Math Operators.
'CTEENTERA', 'CTEFLOTANTE', 'CTETEXTO', #Constant types.
'AND','OR','NOT', # Logical Operators.
'MAYORIGUAL','MAYOR','MENORIGUAL', 'MENOR' ,
'IGUAL', 'DIFERENTE', # Relational Operators
'LLAVEIZQ','LLAVEDER', 'PARENIZQ','PARENDER', 'CORCHIZQ', 'CORCHDER',
'ASIGNACION', 'RETORNO', 'DOSPUNTOS', 'COMA', 'ID'
] + list(reserved.values())

# Tokens' values.
# Math Operators.
t_MAS            = r'\+'
t_MENOS          = r'-'
t_MULTIPLICACION = r'\*'
t_DIVISION       = r'/'
t_MODULO         = r'%'

# Logical Operators.
t_AND = r'&&'
t_OR  = r'\|\|'
t_NOT = r'!'

# Relational Operators
t_MAYORIGUAL = r'>='
t_MAYOR      = r'>'
t_MENORIGUAL = r'<='
t_MENOR      = r'<'
t_IGUAL      = r'=='
t_DIFERENTE  = r'!='

# Other tokens.
t_LLAVEIZQ   = r'{'
t_LLAVEDER   = r'}'
t_PARENIZQ   = r'\('
t_PARENDER   = r'\)'
t_CORCHIZQ   = r'\['
t_CORCHDER   = r'\]'
t_ASIGNACION = r'='
t_RETORNO    = r'->'
t_DOSPUNTOS  = r':'
t_COMA       = r','
t_CTETEXTO  = r'\"[^\"]*\"'

# Define a Comment and discard it.
def t_COMMENT(t):
    r'\#.*'
    pass

# Return a Float token.
def t_CTEFLOTANTE(t):
    r'(\d*[.])\d+'
    t.value = float(t.value)
    return t

# Return an Integer Token.
def t_CTEENTERA(t):
    r'\d+'
    t.value = int(t.value)
    return t

# Return an Identifier.
def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'ID')
    return t

# Ingnored charcteres (Tab, whitespace).
t_ignore = " \t"

# Count the new lines.
def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

# If the string doesn't match any regular expression. Print error and where's
#   the Illegal character.
def t_error(t):
    print("Caracter ilegal '{}' en la línea: {}".format(t.value[0], t.lexer.lineno))
    t.lexer.skip(1)

# Build the Scanner.
lex.lex()

################################################################################
#                                                                              #
# Parser Section.                                                              #
#                                                                              #
################################################################################

# Global Variables.
# These Variables are used in Intemediate code generation.
arrayID = False
varType = None
arraySize = None
readType = None
asignType = None
mainQuadruple = 0
varGlobal = {}
arrayVarGlobal = {}
globalCounter = {ENTERO: 0, FLOTANTE: 0, BOOLEANO: 0, TEXTO: 0}
varLocal = {}
arrayVarLocal = {}
localCounter = {ENTERO: 0,FLOTANTE: 0,BOOLEANO: 0,TEXTO: 0}
temporalCounter = {ENTERO: 0,FLOTANTE: 0,BOOLEANO: 0,TEXTO: 0}
pointersCounter = 0
globalArray = 0
arrayType = None
globalArrayValues = []
constants = {}
functions = {}
functionName = None
functionParameters = {}
returnType = None
functionCounter = 0
expresionReturnType = None
paramsCounter = 0
currentFunctionCall = None
quadruples = []
operandStack = []
operatorStack = []
jumpStack = []
typesStack = []
lineCounter = 0
varCounter = {}

# This function Shows an error message and interrupt the compilation process.
def semanticError(error):
    #strError = error + " en la linea: {}".format(lineCounter)
    sys.exit(error)

# Creates a quadruple for binary operation.
def performOperation():
    rightOperand = operandStack.pop()
    leftOperand = operandStack.pop()
    rightType = typesStack.pop()
    leftType = typesStack.pop()
    operator = operatorStack.pop()
    resultType = getResultType(leftType, operator, rightType)
    if resultType != -1:
        address = getNextTemporalAddress(resultType)
        temporalCounter[resultType] += 1
        addQuadruple(operator, leftOperand, rightOperand, address)
        operandStack.append(address)
        typesStack.append(resultType)
    else:
        semanticError("ERROR: No concuerdan los tipos de dato")

def addQuadruple(operator, variable1, variable2, result):
    global quadruples
    operation = operationCodes[operator]
    if operator != -1:
        quadruples.append({'operation': operation,'variable1': variable1, 'variable2': variable2,'result': result})
    else:
        semanticError("ERROR: Codigo de Operación Inválido")

def fillQuadruples(quadruple, direction):
    global quadruples
    quadruples[quadruple]['result'] = direction

def p_programa(p):
    '''programa : PROGRAMA LLAVEIZQ variablesGlobales addGOTO funciones instrucciones LLAVEDER'''
    # Delete the global Variables and the functions directory.
    # Saves the global variable Counter.
    # print('Operands')
    # print(operandStack)
    # print('operators')
    # print(operatorStack)
    # print('jumps')
    # print(jumpStack)
    # print('types')
    # print(typesStack)
    # print(functions)
    # i = 0
    # for quadruple in quadruples:
    #     var = "{} {}".format(i, quadruple)
    #     print(var)
    #     i = i + 1
    global varCounter
    varCounter = {'globalCounter': globalCounter,
                  'localCounter': localCounter,
                  'temporalCounter': temporalCounter,
                  'pointersCounter': pointersCounter}

def p_addGOTO(p):
    '''addGOTO : empty'''
    addQuadruple('GOTO', '', '', '')
    global mainQuadruple
    mainQuadruple = len(quadruples) - 1

def p_variablesGlobales(p):
    '''variablesGlobales :  declaracionVariableGlobal variablesGlobales
                         | empty'''

def p_declaracionVariableGlobal(p):
    '''declaracionVariableGlobal : declaracionGlobal
                                 | declaracionAsignacionGlobal'''

def p_declaracionGlobal(p):
    '''declaracionGlobal : VAR ID DOSPUNTOS tipo'''
    # Check if the variable is not declared already and save it.
    if p[2] in varGlobal:
        semanticError("ERROR: Variable declarada otra vez")
    else:
        if arraySize:
            address = getNextGlobalAddress(getTypeCode(varType), arraySize)
            arrayVarGlobal[p[2]] = {'type': getTypeCode(varType), 'size': arraySize, 'address': address}
            varGlobal[p[2]] = (getTypeCode(varType), address)
            globalCounter[getTypeCode(varType) - 4] += arraySize
        else:
            varGlobal[p[2]] = (getTypeCode(varType), getNextGlobalAddress(getTypeCode(varType)))
            globalCounter[getTypeCode(varType)] += 1

def p_declaracionAsignacionGlobal(p):
    '''declaracionAsignacionGlobal : VAR ID DOSPUNTOS tipo ASIGNACION expresion'''
    # Check if the variable is not declared already and save it.
    if p[2] in varGlobal:
        semanticError("ERROR: Variable declarada otra vez")
    else:
        if arraySize:
            address = getNextGlobalAddress(getTypeCode(varType), arraySize)
            arrayVarGlobal[p[2]] = {'type': getTypeCode(varType), 'size': arraySize, 'address': address}
            varGlobal[p[2]] = (getTypeCode(varType), address)
            globalCounter[getTypeCode(varType) - 4] += arraySize
            global globalArray
            global arrayType
            global globalArrayValues
            if len(globalArrayValues) > 0:
                global pointersCounter
                for i in range(len(globalArrayValues)):
                    addQuadruple('VERIFYCONST', arrayVarGlobal[p[2]]['size'], i, '')
                    pointerAddress = getNextPointerAddress()
                    pointersCounter += 1
                    addQuadruple('+', arrayVarGlobal[p[2]]['address'], i, pointerAddress)
                    type = ENTERO
                    if arrayVarGlobal[p[2]]['type'] == ARREGLOENTERO:
                        type = ENTERO
                    elif arrayVarGlobal[p[2]]['type'] == ARREGLOFLOTANTE:
                        type = FLOTANTE
                    elif arrayVarGlobal[p[2]]['type'] == ARREGLOBOOLEANO:
                        type = BOOLEANO
                    elif arrayVarGlobal[p[2]]['type'] == ARREGLOTEXTO:
                        type = TEXTO
                    resultType = getResultType(arrayType, '=', type)
                    if resultType != -1:
                        addQuadruple('=', globalArrayValues[i], '', pointerAddress)
                    else:
                        semanticError("ERROR: No concuerdan los tipos de dato")
                globalArray = 0
                arrayType = None
                globalArrayValues = []
            else:
                assignOperand = operandStack.pop()
                assignType = typesStack.pop()
                resultType = getResultType(getTypeCode(varType), '=', assignType)
                if resultType != -1:
                    if assignOperand['size'] == arrayVarLocal[p[2]]['size']:
                        addQuadruple('=',assignOperand , '', arrayVarLocal[p[2]])
                    else:
                        semanticError("ERROR: Los arreglos no son del mismo tamaño")
                else:
                    semanticError("ERROR: No concuerdan los tipos de dato")
        else:
            varGlobal[p[2]] = (getTypeCode(varType), getNextGlobalAddress(getTypeCode(varType)))
            globalCounter[getTypeCode(varType)] += 1
            assignOperand = operandStack.pop()
            assignType = typesStack.pop()
            resultType = getResultType(getTypeCode(varType), '=', assignType)
            if resultType != -1:
                addQuadruple('=', assignOperand, '', varGlobal[p[2]][1])
            else:
                semanticError("ERROR: No concuerdan los tipos de dato")

def p_declaracionVariable(p):
    '''declaracionVariable : declaracion
                            | declaracionAsignacion'''

def p_declaracion(p):
    '''declaracion : VAR ID DOSPUNTOS tipo'''
    # Check if the variable is not declared already and save it.
    if p[2] in varLocal:
        semanticError("ERROR: Variable declarada otra vez")
    else:
        if arraySize:
            address = getNextLocalAddress(getTypeCode(varType), arraySize)
            arrayVarLocal[p[2]] = {'type': getTypeCode(varType), 'size': arraySize, 'address': address}
            varLocal[p[2]] = (getTypeCode(varType), address)
            localCounter[getTypeCode(varType) - 4] += arraySize
        else:
            varLocal[p[2]] = (getTypeCode(varType), getNextLocalAddress(getTypeCode(varType)))
            localCounter[getTypeCode(varType)] += 1

def p_declaracionAsignacion(p):
    '''declaracionAsignacion : VAR ID DOSPUNTOS tipo ASIGNACION expresion
                             | VAR ID DOSPUNTOS tipo ASIGNACION setReadType lectura'''
    # Check if the variable is not declared already and save it.
    # global varLocal
    if p[2] in varLocal:
        semanticError("ERROR: Variable declarada otra vez")
    else:
        if arraySize:
            address = getNextLocalAddress(getTypeCode(varType), arraySize)
            arrayVarLocal[p[2]] = {'type': getTypeCode(varType), 'size': arraySize, 'address': address}
            varLocal[p[2]] = (getTypeCode(varType), address)
            localCounter[getTypeCode(varType) - 4] += arraySize
            global globalArray
            global arrayType
            global globalArrayValues
            if len(globalArrayValues) > 0:
                global pointersCounter
                for i in range(len(globalArrayValues)):
                    addQuadruple('VERIFYCONST', arrayVarLocal[p[2]]['size'], i, '')
                    pointerAddress = getNextPointerAddress()
                    pointersCounter += 1
                    addQuadruple('+', arrayVarLocal[p[2]]['address'], i, pointerAddress)
                    type = ENTERO
                    if arrayVarLocal[p[2]]['type'] == ARREGLOENTERO:
                        type = ENTERO
                    elif arrayVarLocal[p[2]]['type'] == ARREGLOFLOTANTE:
                        type = FLOTANTE
                    elif arrayVarLocal[p[2]]['type'] == ARREGLOBOOLEANO:
                        type = BOOLEANO
                    elif arrayVarLocal[p[2]]['type'] == ARREGLOTEXTO:
                        type = TEXTO
                    resultType = getResultType(arrayType, '=', type)
                    if resultType != -1:
                        addQuadruple('=', globalArrayValues[i], '', pointerAddress)
                    else:
                        semanticError("ERROR: No concuerdan los tipos de dato")
                globalArray = 0
                arrayType = None
                globalArrayValues = []
            else:
                assignOperand = operandStack.pop()
                assignType = typesStack.pop()
                resultType = getResultType(getTypeCode(varType), '=', assignType)
                if resultType != -1:
                    if assignOperand['size'] == arrayVarLocal[p[2]]['size']:
                        addQuadruple('=',assignOperand , '', arrayVarLocal[p[2]])
                    else:
                        semanticError("ERROR: Los arreglos no son del mismo tamaño")
                else:
                    semanticError("ERROR: No concuerdan los tipos de dato")
        else:
            varLocal[p[2]] = (getTypeCode(varType), getNextLocalAddress(getTypeCode(varType)))
            localCounter[getTypeCode(varType)] += 1
            assignOperand = operandStack.pop()
            assignType = typesStack.pop()
            resultType = getResultType(getTypeCode(varType), '=', assignType)
            if resultType != -1:
                addQuadruple('=', assignOperand, '', varLocal[p[2]][1])
            else:
                semanticError("ERROR: No concuerdan los tipos de dato")

def p_setReadType(p):
    '''setReadType : empty'''
    global readType
    readType = getTypeCode(varType)

def p_tipo(p):
    '''tipo : ENTERO
            | FLOTANTE
            | BOOLEANO
            | TEXTO
            | ARREGLO MENOR ENTERO MAYOR CORCHIZQ constante CORCHDER
            | ARREGLO MENOR FLOTANTE MAYOR CORCHIZQ constante CORCHDER
            | ARREGLO MENOR BOOLEANO MAYOR CORCHIZQ constante CORCHDER
            | ARREGLO MENOR TEXTO MAYOR CORCHIZQ constante CORCHDER'''
    # Saves the type in a global variable to use it after.
    global varType
    global arraySize
    if(len(p) >= 5):
        varType = p[1]+p[2]+p[3]+p[4]
        size = operandStack.pop()
        sizeType = typesStack.pop()
        if sizeType != ENTERO:
            semanticError("ERROR: El tamaño del arreglo debe de ser entero")
        else:
            arraySize =  constants[size]['value']
    else:
        arraySize = None
        varType = p[1]

def p_funciones(p):
    '''funciones : funcion funciones
                 | empty'''

def p_funcion(p):
    '''funcion : FUNCION ID checkFuncID PARENIZQ params PARENDER RETORNO tipoRetorno saveVar LLAVEIZQ saveCounter estatutos REGRESA expresionRetorno LLAVEDER'''
    # Save the function in the functions directory.
    global localCounter
    global temporalCounter
    global functionParameters
    if expresionReturnType != getTypeCode(returnType):
        semanticError("ERROR: Función {} con retorno inválido".format(p[2]))
    else:
        addQuadruple('ENDPROC','','','')
        if expresionReturnType == None:
            functions[p[2]] = {'type': getTypeCode(returnType),
                           'params': functionParameters,
                           'counter': functionCounter,
                           'localVarCounter': localCounter,
                           'temporalCounter': temporalCounter}
        else:
            functions[p[2]] = {'type': getTypeCode(returnType),
                           'params': functionParameters,
                           'counter': functionCounter,
                           'localVarCounter': localCounter,
                           'temporalCounter': temporalCounter,
                           'returnAddress': varGlobal[p[2]][1]}
    #Delete the functions parameter and the local variables table.
    localCounter = {ENTERO: 0,FLOTANTE: 0,BOOLEANO: 0,TEXTO: 0}
    temporalCounter = {ENTERO: 0,FLOTANTE: 0,BOOLEANO: 0,TEXTO: 0}
    resetLocalAddresses()
    functionParameters = {}
    global varLocal
    global arrarVarLocal
    varLocal = {}
    arrarVarLocal = {}

def p_checkFuncID(p):
    '''checkFuncID : empty'''
    if p[-1] in functions:
        semanticError("ERROR: Función vuelta a declarar")
    else:
        functions[p[-1]] = {'type': '',
                       'params': functionParameters,
                       'counter': functionCounter,
                       'localVarCounter': None,
                       'temporalCounter': None}
        global functionName
        functionName = p[-1]


def p_saveVar(p):
    '''saveVar : empty'''
    if functionName in varGlobal:
        semanticError("ERROR: Nombre de variable igual al de una función")
    else:
        if getTypeCode(returnType) != None:
            varGlobal[functionName] = (getTypeCode(returnType), getNextGlobalAddress(getTypeCode(returnType)))
            globalCounter[getTypeCode(returnType)] += 1
            functions[functionName] = {'type': getTypeCode(returnType),
                           'params': functionParameters,
                           'counter': functionCounter,
                           'localVarCounter': None,
                           'temporalCounter': None,
                           'returnAddress': varGlobal[functionName][1]}

def p_saveCounter(p):
    '''saveCounter : empty'''
    global functionCounter
    functionCounter = len(quadruples)

def p_params(p):
    '''params : param masParams
              | empty'''

def p_masParams(p):
    '''masParams : COMA param masParams
                 | empty'''

def p_param(p):
    '''param : ID DOSPUNTOS tipo'''
    # Check if the function parameter is not repeated and save it.
    if p[1] in functionParameters:
        semanticError("ERROR: Parámetro duplicado")
    else:
        address = getNextLocalAddress(getTypeCode(varType))
        functionParameters[p[1]] = {'type' : getTypeCode(varType), 'address' : address}
        varLocal[p[1]] = (getTypeCode(varType), address)
        localCounter[getTypeCode(varType)] += 1

def p_tipoRetorno(p):
    '''tipoRetorno : ENTERO
                   | FLOTANTE
                   | BOOLEANO
                   | TEXTO
                   | ARREGLO MENOR ENTERO MAYOR
                   | ARREGLO MENOR FLOTANTE MAYOR
                   | ARREGLO MENOR BOOLEANO MAYOR
                   | ARREGLO MENOR TEXTO MAYOR
                   | NADA '''
    # Saves the return type of the function to use it after.
    global returnType
    if(len(p) == 5):
        returnType = p[1]+p[2]+p[3]+p[4]
    else:
        returnType = p[1]

def p_estatutos(p):
    '''estatutos : estatuto estatutos
                 | empty'''

def p_estatuto(p):
    '''estatuto : declaracionVariable
                | asignacion
                | condicion
                | ciclo
                | escritura
                | llamada'''

def p_asignacion(p):
    '''asignacion : ID setReadTypeAsign ASIGNACION expresionLee
                  | casillaArreglo setReadTypeAsign ASIGNACION expresionLee'''
    # Check if exists in local variables.
    global globalArray
    global arrayType
    global globalArrayValues
    global pointersCounter
    if (p[1] in arrayVarLocal) and (len(globalArrayValues) > 0):
        for i in range(len(globalArrayValues)):
            addQuadruple('VERIFYCONST', arrayVarLocal[p[1]]['size'], i, '')
            pointerAddress = getNextPointerAddress()
            pointersCounter += 1
            addQuadruple('+', arrayVarLocal[p[1]]['address'], i, pointerAddress)
            type = ENTERO
            if arrayVarLocal[p[1]]['type'] == ARREGLOENTERO:
                type = ENTERO
            elif arrayVarLocal[p[1]]['type'] == ARREGLOFLOTANTE:
                type = FLOTANTE
            elif arrayVarLocal[p[1]]['type'] == ARREGLOBOOLEANO:
                type = BOOLEANO
            elif arrayVarLocal[p[1]]['type'] == ARREGLOTEXTO:
                type = TEXTO
            resultType = getResultType(arrayType, '=', type)
            if resultType != -1:
                addQuadruple('=', globalArrayValues[i], '', pointerAddress)
            else:
                semanticError("ERROR: No concuerdan los tipos de dato")
        globalArray = 0
        arrayType = None
        globalArrayValues = []
    elif p[1] in arrayVarGlobal and (len(globalArrayValues) > 0):
        for i in range(len(globalArrayValues)):
            addQuadruple('VERIFYCONST', arrayVarGlobal[p[1]]['size'], i, '')
            pointerAddress = getNextPointerAddress()
            pointersCounter += 1
            addQuadruple('+', arrayVarGlobal[p[1]]['address'], i, pointerAddress)
            type = ENTERO
            if arrayVarGlobal[p[1]]['type'] == ARREGLOENTERO:
                type = ENTERO
            elif arrayVarGlobal[p[1]]['type'] == ARREGLOFLOTANTE:
                type = FLOTANTE
            elif arrayVarGlobal[p[1]]['type'] == ARREGLOBOOLEANO:
                type = BOOLEANO
            elif arrayVarGlobal[p[1]]['type'] == ARREGLOTEXTO:
                type = TEXTO
            resultType = getResultType(arrayType, '=', type)
            if resultType != -1:
                addQuadruple('=', globalArrayValues[i], '', pointerAddress)
            else:
                semanticError("ERROR: No concuerdan los tipos de dato")
        globalArray = 0
        arrayType = None
        globalArrayValues = []
    elif p[1] in arrayVarLocal:
        assignOperand = operandStack.pop()
        assignType = typesStack.pop()
        resultType = getResultType(varLocal[p[1]][0], '=', assignType)
        if resultType != -1:
            addQuadruple('=', assignOperand, '', arrayVarLocal[p[1]])
        else:
            semanticError("ERROR: No concuerdan los tipos de dato")
    elif p[1] in arrayVarGlobal:
        assignOperand = operandStack.pop()
        assignType = typesStack.pop()
        resultType = getResultType(varGlobal[p[1]][0], '=', assignType)
        if resultType != -1:
            addQuadruple('=', assignOperand, '', arrayVarGlobal[p[1]])
        else:
            semanticError("ERROR: No concuerdan los tipos de dato")
    elif p[1] in varLocal:
        assignOperand = operandStack.pop()
        assignType = typesStack.pop()
        resultType = getResultType(varLocal[p[1]][0], '=', assignType)
        if resultType != -1:
            addQuadruple('=', assignOperand, '', varLocal[p[1]][1])
        else:
            semanticError("ERROR: No concuerdan los tipos de dato")
    # Check if exists in global variables.
    elif p[1] in varGlobal:
        assignOperand = operandStack.pop()
        assignType = typesStack.pop()
        resultType = getResultType(varGlobal[p[1]][0], '=', assignType)
        if resultType != -1:
            addQuadruple('=', assignOperand, '', varGlobal[p[1]][1])
        else:
            semanticError("ERROR: No concuerdan los tipos de dato")
    else:
        if arrayID == True:
            toAssignOperand = operandStack.pop()
            toAssignType = typesStack.pop()
            assignOperand = operandStack.pop()
            assignType = typesStack.pop()
            resultType = getResultType(assignType, '=', toAssignType)
            if resultType != -1:
                addQuadruple('=', toAssignOperand, '', assignOperand)
            else:
                semanticError("ERROR: No concuerdan los tipos de dato")
        else:
            semanticError("ERROR: Variable no declarada")

def p_setReadTypeAsign(p):
    '''setReadTypeAsign : empty'''
    global readType
    if p[-1] in varLocal:
        readType = varLocal[p[-1]][0]
    elif p[-1] in varGlobal:
        readType = varGlobal[p[-1]][0]

def p_casillaArreglo(p):
    '''casillaArreglo : ID CORCHIZQ expresion CORCHDER'''
    accessOperand = operandStack.pop()
    accessType = typesStack.pop()
    global arrayID
    global pointersCounter
    if accessType != ENTERO:
        semanticError('ERROR: Acceso a arreglo no es entero')
    else:
        if p[1] in arrayVarLocal:
            arrayID = True
            addQuadruple('VERIFY', arrayVarLocal[p[1]]['size'], accessOperand, '')
            pointerAddress = getNextPointerAddress()
            pointersCounter += 1
            addQuadruple('+', arrayVarLocal[p[1]]['address'], accessOperand, pointerAddress)
            operandStack.append(pointerAddress)
            type = ENTERO
            if arrayVarLocal[p[1]] == ARREGLOENTERO:
                type = ENTERO
            elif arrayVarLocal[p[1]] == ARREGLOFLOTANTE:
                type = FLOTANTE
            elif arrayVarLocal[p[1]] == ARREGLOBOOLEANO:
                type = BOOLEANO
            elif arrayVarLocal[p[1]] == ARREGLOTEXTO:
                type = TEXTO
            typesStack.append(type)
        elif p[1] in arrayVarGlobal:
            arrayID = True
            addQuadruple('VERIFY', arrayVarGlobal[p[1]]['size'], accessOperand, '')
            pointerAddress = getNextPointerAddress()
            pointersCounter += 1
            addQuadruple('+', arrayVarGlobal[p[1]]['address'], accessOperand, pointerAddress)
            type = ENTERO
            operandStack.append(pointerAddress)
            if arrayVarGlobal[p[1]] == ARREGLOENTERO:
                type = ENTERO
            elif arrayVarGlobal[p[1]] == ARREGLOFLOTANTE:
                type = FLOTANTE
            elif arrayVarGlobal[p[1]] == ARREGLOBOOLEANO:
                type = BOOLEANO
            elif arrayVarGlobal[p[1]] == ARREGLOTEXTO:
                type = TEXTO
            typesStack.append(type)
        else:
            arrayID = False
            semanticError('ERROR: Variable no declarada')

def p_condicion(p):
    '''condicion : SI expresion cond1 bloque else'''
    end = jumpStack.pop()
    fillQuadruples(end, len(quadruples))

def p_cond1(p):
    '''cond1 : empty'''
    type = typesStack.pop()
    if type != getTypeCode('Booleano'):
        semanticError("ERROR: la expresion no es booleana")
    else:
        result = operandStack.pop()
        addQuadruple('GOTOF',result,'','')
        jumpStack.append(len(quadruples) - 1)

def p_else(p):
    '''else : SINO cond3 bloque
            | empty'''

def p_cond3(p):
    '''cond3 : empty'''
    addQuadruple('GOTO','','','')
    false = jumpStack.pop()
    jumpStack.append(len(quadruples) - 1)
    fillQuadruples(false, len(quadruples))

def p_bloque(p):
    '''bloque : LLAVEIZQ estatutos LLAVEDER'''

def p_ciclo(p):
    '''ciclo : MIENTRAS ciclo1 expresion ciclo2 bloque'''
    end = jumpStack.pop()
    returnAddress = jumpStack.pop()
    addQuadruple('GOTO', '', '', returnAddress)
    fillQuadruples(end, len(quadruples))

def p_ciclo1(p):
    '''ciclo1 : empty'''
    jumpStack.append(len(quadruples))

def p_ciclo2(p):
    '''ciclo2 : empty'''
    type = typesStack.pop()
    if type != getTypeCode('Booleano'):
        semanticError("ERROR: la expresion no es booleana")
    else:
        result = operandStack.pop()
        addQuadruple('GOTOF',result,'','')
        jumpStack.append(len(quadruples) -1)


def p_escritura(p):
    '''escritura : IMPRIME PARENIZQ expresion PARENDER'''
    assignOperand = operandStack.pop()
    type = typesStack.pop()
    addQuadruple('PRINT',assignOperand, '', '')

def p_lectura(p):
    '''lectura : LEE PARENIZQ PARENDER'''
    address = getNextTemporalAddress(readType)
    operandStack.append(address)
    typesStack.append(readType)
    temporalCounter[readType] += 1
    addQuadruple('READ', '', '', address)

def p_cuentaArreglo(p):
    '''cuentaArreglo : CUENTA PARENIZQ expresion PARENDER'''
    global arrayType
    global globalArrayValues
    global constants
    if arrayType != None:
        address = getNextConstantAddress(getTypeCode("Entero"))
        constants[address] = {'value':len(globalArrayValues),
                            'type':getTypeCode("Entero"),
                            'dir': address}
        operandStack.append(constants[address]['dir'])
        typesStack.append(getTypeCode("Entero"))
        arrayType = None
        globalArrayValues = {}
    elif len(operandStack) > 0:
        operand = operandStack.pop()
        type = typesStack.pop()
        if type < 5:
            semanticError('ERROR: Expresion debe ser de tipo Arreglo')
        else:
            address = getNextConstantAddress(getTypeCode("Entero"))
            constants[address] = {'value':operand['size'],
                                'type':getTypeCode("Entero"),
                                'dir': address}
            operandStack.append(constants[address]['dir'])
            typesStack.append(getTypeCode("Entero"))

def p_llamada(p):
    '''llamada : ID checkID PARENIZQ paramsLLamada PARENDER'''
    global paramsCounter
    if paramsCounter != len(functions[p[1]]['params']):
        semanticError('ERROR: Parametros no concuerdan')
    else:
        paramsCounter = 0
        addQuadruple('GOTOSUB', p[1], '', '')
        if functions[p[1]]['type'] != None:
            address = getNextTemporalAddress(functions[p[1]]['type'])
            addQuadruple('=', functions[p[1]]['returnAddress'], '', address)
            temporalCounter[functions[p[1]]['type']] += 1
            operandStack.append(address)
            typesStack.append(functions[p[1]]['type'])

def p_checkID(p):
    '''checkID : empty'''
    if not p[-1] in functions:
        semanticError('ERROR: Llamada a función no declarada')
    else:
        global currentFunctionCall
        currentFunctionCall = p[-1]
        addQuadruple('ERA', p[-1], '', '')


def p_paramsLLamada(p):
    '''paramsLLamada : expresion saveExp masParamsLlamada
                     | empty'''

def p_masParamsLlamada(p):
    '''masParamsLlamada : COMA expresion saveExp masParamsLlamada
                        | empty'''

def p_saveExp(p):
    '''saveExp : empty'''
    global paramsCounter
    argument = operandStack.pop()
    argumentType = typesStack.pop()
    if list(functions[currentFunctionCall]['params'].values())[paramsCounter]['type'] != argumentType:
        semanticError('ERROR: El tipo de parámetro no concuerda')
    else:
        address = list(functions[currentFunctionCall]['params'].values())[paramsCounter]['address']
        addQuadruple('PARAMETER', argument, '', address)
        paramsCounter = paramsCounter + 1

def p_constante(p):
    '''constante :  ID opStackID
                 | CTEENTERA opStackCTEEntera
                 | CTEFLOTANTE opStackCTEFlotante
                 | MENOS CTEENTERA opStackCTENegEntera
                 | MENOS CTEFLOTANTE opStackCTENegFlotante
                 | CTETEXTO opStackTexto
                 | VERDADERO opStackBooleano
                 | FALSO opStackBooleano
                 | cteArreglo
                 | casillaArreglo
                 | llamada opStackLlamada
                 | cuentaArreglo'''

def p_opStackID(p):
    '''opStackID : empty'''
    if p[-1] in arrayVarLocal:
        operandStack.append(arrayVarLocal[p[-1]])
        typesStack.append(arrayVarLocal[p[-1]]['type'])

    elif p[-1] in arrayVarGlobal:
        operandStack.append(arrayVarGlobal[p[-1]])
        typesStack.append(arrayVarGlobal[p[-1]]['type'])
    else:
        if p[-1] in varLocal:
            operandStack.append(varLocal[p[-1]][1])
            typesStack.append(varLocal[p[-1]][0])
        elif p[-1] in varGlobal:
            operandStack.append(varGlobal[p[-1]][1])
            typesStack.append(varGlobal[p[-1]][0])
        else:
            semanticError("ERROR: Variable no declarada {}".format(p[-1]))

def p_opStackCTEEntera(p):
    '''opStackCTEEntera : empty'''
    address = getNextConstantAddress(getTypeCode("Entero"))
    global constants
    constants[address] = {'value':p[-1],
                        'type':getTypeCode("Entero"),
                        'dir': address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Entero"))

def p_opStackCTEFlotante(p):
    '''opStackCTEFlotante : empty'''
    address = getNextConstantAddress(getTypeCode("Flotante"))
    global constants
    constants[address] = {'value':p[-1],
                          'type':getTypeCode("Flotante"),
                          'dir':address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Flotante"))

def p_opStackCTENegEntera(p):
    '''opStackCTENegEntera : empty'''
    global constants
    address = getNextConstantAddress(getTypeCode("Entero"))
    constante = p[-1] * -1
    constants[address] = {'value': constante,
                          'type': getTypeCode("Entero"),
                          'dir': address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Entero"))

def p_opStackCTENegFlotante(p):
    '''opStackCTENegFlotante : empty'''
    global constants
    constante = p[-1] * -1
    address = getNextConstantAddress(getTypeCode("Flotante"))
    constants[address] = {'value':constante,
                          'type':getTypeCode("Flotante"),
                          'dir':address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Flotante"))

def p_opStackTexto(p):
    '''opStackTexto : empty'''
    global constants
    address = getNextConstantAddress(getTypeCode("Texto"))
    constants[address] = {'value':p[-1].replace('"', ''),
                        'type':getTypeCode("Texto"),
                        'dir':address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Texto"))

def p_opStackBooleano(p):
    '''opStackBooleano : empty'''
    global constants
    address = getNextConstantAddress(getTypeCode("Booleano"))
    value = True if p[-1] == 'verdadero' else False
    constants[address] = {'value':value,
                        'type':getTypeCode("Booleano"),
                        'dir': address}
    operandStack.append(constants[address]['dir'])
    typesStack.append(getTypeCode("Booleano"))

def p_opStackLlamada(p):
    '''opStackLlamada : empty'''
    if functions[currentFunctionCall]['type'] == None:
        semanticError('ERROR: la función no tiene valor de retorno')

def p_cteArreglo(p):
    '''cteArreglo : CORCHIZQ constantesArreglo CORCHDER'''
    global arrayType
    arrayType = None
    global globalArrayValues
    for i in range(0, globalArray):
        operand = operandStack.pop()
        type = typesStack.pop()
        if i == 0:
            arrayType = type
        else:
            if type != arrayType:
                semanticError("ERROR: Arreglo con diferentes tipos de dato")
                break
        globalArrayValues.insert(0, operand)

def p_addCounter(p):
    '''addCounter : empty'''
    global globalArray
    globalArray += 1

def p_constantesArreglo(p):
    '''constantesArreglo : expresion addCounter masConstantes'''

def p_masConstantes(p):
    '''masConstantes : COMA expresion addCounter masConstantes
                     | empty'''

def p_expresionRetorno(p):
    '''expresionRetorno : expresion
                        | NADA'''
    global expresionReturnType
    if p[1] == 'Nada':
        expresionReturnType = None
    else:
        expresionReturnType = typesStack.pop()
        operand = operandStack.pop()
        addQuadruple('RETURN', operand, '', '')

def p_expresionLee(p):
    '''expresionLee : lectura
                    | expresion'''

def p_expresion(p):
    '''expresion : expresionLogica masExpLogicas'''

def p_masExpLogicas(p):
    '''masExpLogicas : operadorLogico expresionLogica checkStackLogica masExpLogicas
                     | empty'''

def p_checkStackLogica(p):
    "checkStackLogica : empty"
    if operatorStack[len(operatorStack) - 1] == '&&' or operatorStack[len(operatorStack) - 1] == '||':
        performOperation()

def p_expresionLogica(p):
    '''expresionLogica : expresionRelacional masExpRelacionales
                       | NOT expresionRelacional masExpRelacionales'''
    if p[1] == '!':
        notOperand = operandStack.pop()
        notType = typesStack.pop()
        if notType != getTypeCode("Booleano"):
            semanticError("ERROR: No concuerdan los tipos de dato")
        else:
            temporalCounter[getTypeCode(varType)] += 1
            address = getNextTemporalAddress(getTypeCode("Booleano"))
            addQuadruple('!', notOperand, '', address)
            operandStack.append(address)
            typesStack.append(notType)

def p_masExpRelacionales(p):
    '''masExpRelacionales : operadorRelacional expresionRelacional checkStackRel
                          | empty'''

def p_checkStackRel(p):
    "checkStackRel : empty"
    if operatorStack[len(operatorStack) - 1] == '>' or operatorStack[len(operatorStack) - 1] == '<' or operatorStack[len(operatorStack) - 1] == '>=' or operatorStack[len(operatorStack) - 1] == '<=' or operatorStack[len(operatorStack) - 1] == '==' or operatorStack[len(operatorStack) - 1] == '!=':
        performOperation()

def p_expresionRelacional(p):
    '''expresionRelacional : termino sumasRestas'''

def p_sumasRestas(p):
    '''sumasRestas : MAS saveOp termino checkStackSumaResta sumasRestas
                   | MENOS saveOp termino checkStackSumaResta sumasRestas
                   | empty'''

def p_saveOp(p):
    '''saveOp : empty'''
    operatorStack.append(p[-1])

def p_checkStackSumaResta(p):
    "checkStackSumaResta : empty"
    if operatorStack[len(operatorStack) - 1] == '+' or operatorStack[len(operatorStack) - 1] == '-':
        performOperation()

def p_termino(p):
    '''termino : factor operaciones'''

def p_operaciones(p):
    '''operaciones : MULTIPLICACION saveOp factor checkStack operaciones
                   | DIVISION saveOp factor checkStack operaciones
                   | MODULO saveOp factor checkStack operaciones
                   | empty'''

def p_checkStack(p):
    "checkStack : empty"
    if operatorStack[len(operatorStack) - 1] == '*' or operatorStack[len(operatorStack) - 1] == '/' or operatorStack[len(operatorStack) - 1] == '%':
        performOperation()

def p_factor(p):
    '''factor : PARENIZQ pushFalseBottom expresion PARENDER
              | constante'''
    if p[1] == '(':
        operatorStack.pop()

def p_pushFalseBottom(p):
    '''pushFalseBottom : empty'''
    operatorStack.append('@')

def p_operadorLogico(p):
    '''operadorLogico : AND
                      | OR'''
    operatorStack.append(p[1])

def p_operadorRelacional(p):
    '''operadorRelacional : MAYOR
                          | MAYORIGUAL
                          | MENOR
                          | MENORIGUAL
                          | IGUAL
                          | DIFERENTE'''
    operatorStack.append(p[1])

def p_instrucciones(p):
    '''instrucciones : INSTRUCCIONES gotoMain bloque'''

def p_gotoMain(p):
    '''gotoMain : empty'''
    fillQuadruples(mainQuadruple, len(quadruples))

def p_empty(p):
    '''empty :'''

# If there's error, print the token and interrupt the compilation process.
def p_error(p):
    sys.exit("ERROR {}".format(p))

yacc.yacc()

if __name__ == '__main__':
    # Check for file
    if len(sys.argv) > 2:
        fileName = sys.argv[1]
        mode = sys.argv[2]
        # Compile and Execute code
        if mode == '-c':
            # Open source code file
            print('Leyendo archivo...')
            file = open(fileName, 'r')
            code = file.read()
            file.close()

            # Parse the data
            print('Compilando código fuente...')
            yacc.parse(code)

            print('Guardando código objeto...')
            # Save compiled code
            objectCode = "{ 'functions' : %r, 'quadruples' : %r, 'constants' : %r, 'varCounter' : %r}" % (functions,quadruples,constants,varCounter)
            objectCodeFile = open(fileName.replace('tef', 'tfo'),'w')
            objectCodeFile.write(objectCode)
            objectCodeFile.close()
            #Execute virtual machine
            print('Ejecutando máquina virtual...')
            executeCode(functions, quadruples, constants, varCounter)

        # Run compiled code.
        elif mode == '-r':
            # Open object code file
            print('Leyendo archivo...')
            file = open(fileName, 'r')
            code = file.read()
            file.close()

            # Decoding code
            objectCode = literal_eval(code)

            #Execute virtual machine
            print('Ejecutando máquina virtual...')
            executeCode(objectCode['functions'], objectCode['quadruples'], objectCode['constants'], objectCode['varCounter'])
        else:
            sys.exit("ERROR: Modo de execución inexistente usa '-c' para compilar y ejecutar y '-r' para ejecutar")
    else:
        sys.exit("ERROR: Modo de uso: python3 temporalFinal.py 'nombreArchivo' (-c | -r)")
