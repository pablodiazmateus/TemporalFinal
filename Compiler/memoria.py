ENTERO          = 1
FLOTANTE        = 2
BOOLEANO        = 3
TEXTO           = 4
ARREGLOENTERO   = 5
ARREGLOFLOTANTE = 6
ARREGLOBOOLEANO = 7
ARREGLOTEXTO    = 8
################################################################################
#                                                                              #
# Memory Addresses                                                             #
#                                                                              #
################################################################################
# Global Addresses
initialGlobalAddress = {ENTERO   : 10000,
                        FLOTANTE : 12500,
                        BOOLEANO : 15000,
                        TEXTO    : 17500}

globalVariablesAddress = {ENTERO   : initialGlobalAddress[ENTERO],
                          FLOTANTE : initialGlobalAddress[FLOTANTE],
                          BOOLEANO : initialGlobalAddress[BOOLEANO],
                          TEXTO    : initialGlobalAddress[TEXTO]}

#Local Addresses
initialLocalVariablesAddress = {ENTERO   : 20000,
                                FLOTANTE : 22500,
                                BOOLEANO : 25000,
                                TEXTO    : 27500}

localVariablesAddress = {ENTERO   : initialLocalVariablesAddress[ENTERO],
                         FLOTANTE : initialLocalVariablesAddress[FLOTANTE],
                         BOOLEANO : initialLocalVariablesAddress[BOOLEANO],
                         TEXTO    : initialLocalVariablesAddress[TEXTO]}

#Temporal Addresses
initialTemporalVariablesAddress = {ENTERO   : 30000,
                                   FLOTANTE : 32500,
                                   BOOLEANO : 35000,
                                   TEXTO    : 37500}

temporalVariablesAddress = {ENTERO   : initialTemporalVariablesAddress[ENTERO],
                            FLOTANTE : initialTemporalVariablesAddress[FLOTANTE],
                            BOOLEANO : initialTemporalVariablesAddress[BOOLEANO],
                            TEXTO    : initialTemporalVariablesAddress[TEXTO]}

#Constant Initial Addresses
initialConstantsAddress = {ENTERO   : 40000,
                           FLOTANTE : 42500,
                           BOOLEANO : 45000,
                           TEXTO    : 47500}

constantsAddress = {ENTERO   : initialConstantsAddress[ENTERO],
                    FLOTANTE : initialConstantsAddress[FLOTANTE],
                    BOOLEANO : initialConstantsAddress[BOOLEANO],
                    TEXTO    : initialConstantsAddress[TEXTO]}

initialPointerAddress = 50000

pointerAddress = initialPointerAddress

def resetLocalAddresses():
    global localVariablesAddress
    global temporalVariablesAddress

    localVariablesAddress = {ENTERO   : initialLocalVariablesAddress[ENTERO],
                             FLOTANTE : initialLocalVariablesAddress[FLOTANTE],
                             BOOLEANO : initialLocalVariablesAddress[BOOLEANO],
                             TEXTO    : initialLocalVariablesAddress[TEXTO]}

    temporalVariablesAddress = {ENTERO   : initialTemporalVariablesAddress[ENTERO],
                                FLOTANTE : initialTemporalVariablesAddress[FLOTANTE],
                                BOOLEANO : initialTemporalVariablesAddress[BOOLEANO],
                                TEXTO    : initialTemporalVariablesAddress[TEXTO]}


def getNextPointerAddress():
    global pointerAddress
    addressToReturn = pointerAddress
    pointerAddress += 1
    return addressToReturn

def getNextGlobalAddress(typeCode, quantity=1):
    if typeCode == ARREGLOENTERO:
        addressToReturn = globalVariablesAddress[ENTERO]
        globalVariablesAddress[ENTERO] += quantity
        return addressToReturn
    elif typeCode == ARREGLOFLOTANTE:
        addressToReturn = globalVariablesAddress[FLOTANTE]
        globalVariablesAddress[FLOTANTE] += quantity
        return addressToReturn
    elif typeCode == ARREGLOBOOLEANO:
        addressToReturn = globalVariablesAddress[BOOLEANO]
        globalVariablesAddress[BOOLEANO] += quantity
        return addressToReturn
    elif typeCode == ARREGLOTEXTO:
        addressToReturn = globalVariablesAddress[TEXTO]
        globalVariablesAddress[TEXTO] += quantity
        return addressToReturn
    else:
        addressToReturn = globalVariablesAddress[typeCode]
        globalVariablesAddress[typeCode] += 1
        return addressToReturn

def getNextLocalAddress(typeCode, quantity=1):
    if typeCode == ARREGLOENTERO:
        addressToReturn = localVariablesAddress[ENTERO]
        localVariablesAddress[ENTERO] += quantity
        return addressToReturn
    elif typeCode == ARREGLOFLOTANTE:
        addressToReturn = localVariablesAddress[FLOTANTE]
        localVariablesAddress[FLOTANTE] += quantity
        return addressToReturn
    elif typeCode == ARREGLOBOOLEANO:
        addressToReturn = localVariablesAddress[BOOLEANO]
        localVariablesAddress[BOOLEANO] += quantity
        return addressToReturn
    elif typeCode == ARREGLOTEXTO:
        addressToReturn = localVariablesAddress[TEXTO]
        localVariablesAddress[TEXTO] += quantity
        return addressToReturn
    else:
        addressToReturn = localVariablesAddress[typeCode]
        localVariablesAddress[typeCode] += 1
        return addressToReturn

def getNextTemporalAddress(typeCode, quantity=1):
    if typeCode == ARREGLOENTERO:
        addressToReturn = temporalVariablesAddress[ENTERO]
        temporalVariablesAddress[ENTERO] += 1
        return addressToReturn
    elif typeCode == ARREGLOFLOTANTE:
        addressToReturn = temporalVariablesAddress[FLOTANTE]
        temporalVariablesAddress[FLOTANTE] += 1
        return addressToReturn
    elif typeCode == ARREGLOBOOLEANO:
        addressToReturn = temporalVariablesAddress[BOOLEANO]
        temporalVariablesAddress[BOOLEANO] += 1
        return addressToReturn
    elif typeCode == ARREGLOTEXTO:
        addressToReturn = temporalVariablesAddress[TEXTO]
        temporalVariablesAddress[TEXTO] += 1
        return addressToReturn
    else:
        addressToReturn = temporalVariablesAddress[typeCode]
        temporalVariablesAddress[typeCode] += 1
        return addressToReturn

def getNextConstantAddress(typeCode, quantity=1):
    if typeCode == ARREGLOENTERO:
        pass
    elif typeCode == ARREGLOFLOTANTE:
        pass
    elif typeCode == ARREGLOBOOLEANO:
        pass
    elif typeCode == ARREGLOTEXTO:
        pass
    else:
        addressToReturn = constantsAddress[typeCode]
        constantsAddress[typeCode] += 1
        return addressToReturn

################################################################################
#                                                                              #
# Execution memory                                                             #
#                                                                              #
################################################################################

class Memoria:
    def __init__(self, globalVariables, localVariables, temporalVariables, pointers):
        # Global addresses
        self.integerGlobalVariables = [None] * globalVariables[ENTERO]
        self.floatGlobalVariables = [None] * globalVariables[FLOTANTE]
        self.boolGlobalVariables = [None] * globalVariables[BOOLEANO]
        self.stringGlobalVariables = [None] * globalVariables[TEXTO]

        # Local addresses
        self.integerLocalVariables = [None] * localVariables[ENTERO]
        self.floatLocalVariables = [None] * localVariables[FLOTANTE]
        self.boolLocalVariables = [None] * localVariables[BOOLEANO]
        self.stringLocalVariables = [None] * localVariables[TEXTO]

        # Temporal addresses
        self.integerTemporalVariables = [None] * temporalVariables[ENTERO]
        self.floatTemporalVariables = [None] * temporalVariables[FLOTANTE]
        self.boolTemporalVariables = [None] * temporalVariables[BOOLEANO]
        self.stringTemporalVariables = [None] * temporalVariables[TEXTO]

        # Pointers
        self.pointers = [None] * pointers

        # Memory Stack. Saves previous memory state.
        self.memoryStack = []

    def functionMemory(self, localVarCounter, temporalVarCounter):
        # Local addresses
        self.funcIntegerLocalVariables = [None] * localVarCounter[ENTERO]
        self.funcFloatLocalVariables = [None] * localVarCounter[FLOTANTE]
        self.funcBoolLocalVariables = [None] * localVarCounter[BOOLEANO]
        self.funcStringLocalVariables = [None] * localVarCounter[TEXTO]

        # Temporal addresses
        self.funcIntegerTemporalVariables = [None] * temporalVarCounter[ENTERO]
        self.funcFloatTemporalVariables = [None] * temporalVarCounter[FLOTANTE]
        self.funcBoolTemporalVariables = [None] * temporalVarCounter[BOOLEANO]
        self.funcStringTemporalVariables = [None] * temporalVarCounter[TEXTO]

    def saveMemoryState(self):
        memoryToSave = {'localVariables' : {
        ENTERO : self.integerLocalVariables,
        FLOTANTE : self.floatLocalVariables,
        BOOLEANO : self.boolLocalVariables,
        TEXTO : self.stringLocalVariables
        }, 'temporalVariables' : {
        ENTERO : self.integerTemporalVariables,
        FLOTANTE : self.floatTemporalVariables,
        BOOLEANO : self.boolTemporalVariables,
        TEXTO : self.stringTemporalVariables
        }}

        self.memoryStack.append(memoryToSave)

        # Local addresses
        self.integerLocalVariables = self.funcIntegerLocalVariables
        self.floatLocalVariables = self.funcFloatLocalVariables
        self.boolLocalVariables = self.funcBoolLocalVariables
        self.stringLocalVariables = self.funcStringLocalVariables

        # Temporal addresses
        self.integerTemporalVariables = self.funcIntegerTemporalVariables
        self.floatTemporalVariables = self.funcFloatTemporalVariables
        self.boolTemporalVariables = self.funcBoolTemporalVariables
        self.stringTemporalVariables = self.funcStringTemporalVariables

    def recoverMemoryState(self):
        memoryToRecover = self.memoryStack.pop()
        # Local addresses
        self.integerLocalVariables = memoryToRecover['localVariables'][ENTERO]
        self.floatLocalVariables = memoryToRecover['localVariables'][FLOTANTE]
        self.boolLocalVariables = memoryToRecover['localVariables'][BOOLEANO]
        self.stringLocalVariables = memoryToRecover['localVariables'][TEXTO]

        # Temporal addresses
        self.integerTemporalVariables = memoryToRecover['temporalVariables'][ENTERO]
        self.floatTemporalVariables = memoryToRecover['temporalVariables'][FLOTANTE]
        self.boolTemporalVariables = memoryToRecover['temporalVariables'][BOOLEANO]
        self.stringTemporalVariables = memoryToRecover['temporalVariables'][TEXTO]

    def storeParam(self, address, value):
        scope = self.getScope(address)
        varType = self.getVarType(address, scope)
        if varType == ENTERO:
            self.funcIntegerLocalVariables[address - initialLocalVariablesAddress[ENTERO]] = value
        elif varType == FLOTANTE:
            self.funcFloatLocalVariables[address - initialLocalVariablesAddress[FLOTANTE]] = value
        elif varType == BOOLEANO:
            self.funcBoolLocalVariables[address - initialLocalVariablesAddress[BOOLEANO]] = value
        else:
            self.funcStringLocalVariables[address - initialLocalVariablesAddress[TEXTO]] = value

    def getValue(self, address, constants):
        if address == '':
            return
        else:
            scope = self.getScope(address)
            varType = self.getVarType(address, scope)
            if scope == 'global':
                if varType == ENTERO:
                    return self.integerGlobalVariables[address - initialGlobalAddress[ENTERO]]
                elif varType == FLOTANTE:
                    return self.floatGlobalVariables[address - initialGlobalAddress[FLOTANTE]]
                elif varType == BOOLEANO:
                    return self.boolGlobalVariables[address - initialGlobalAddress[BOOLEANO]]
                else:
                    return self.stringGlobalVariables[address - initialGlobalAddress[TEXTO]]

            elif scope == 'local':
                if varType == ENTERO:
                    return self.integerLocalVariables[address - initialLocalVariablesAddress[ENTERO]]
                elif varType == FLOTANTE:
                    return self.floatLocalVariables[address - initialLocalVariablesAddress[FLOTANTE]]
                elif varType == BOOLEANO:
                    return self.boolLocalVariables[address - initialLocalVariablesAddress[BOOLEANO]]
                else:
                    return self.stringLocalVariables[address - initialLocalVariablesAddress[TEXTO]]

            elif scope == 'temporal':
                if varType == ENTERO:
                    return self.integerTemporalVariables[address - initialTemporalVariablesAddress[ENTERO]]
                elif varType == FLOTANTE:
                    return self.floatTemporalVariables[address - initialTemporalVariablesAddress[FLOTANTE]]
                elif varType == BOOLEANO:
                    return self.boolTemporalVariables[address - initialTemporalVariablesAddress[BOOLEANO]]
                else:
                    return self.stringTemporalVariables[address - initialTemporalVariablesAddress[TEXTO]]

            elif scope == 'constants':
                return constants[address]['value']

            else:
                return self.getValue(self.pointers[address - initialPointerAddress], constants)

    def saveValue(self, address, value, saveAddress=False):
        if address == '':
            return
        else:
            scope = self.getScope(address)
            varType = self.getVarType(address, scope)
            if scope == 'global':
                if varType == ENTERO:
                    self.integerGlobalVariables[address - initialGlobalAddress[ENTERO]] = value
                elif varType == FLOTANTE:
                    self.floatGlobalVariables[address - initialGlobalAddress[FLOTANTE]] = value
                elif varType == BOOLEANO:
                    self.boolGlobalVariables[address - initialGlobalAddress[BOOLEANO]] = value
                else:
                    self.stringGlobalVariables[address - initialGlobalAddress[TEXTO]] = value

            elif scope == 'local':
                if varType == ENTERO:
                    self.integerLocalVariables[address - initialLocalVariablesAddress[ENTERO]] = value
                elif varType == FLOTANTE:
                    self.floatLocalVariables[address - initialLocalVariablesAddress[FLOTANTE]] = value
                elif varType == BOOLEANO:
                    self.boolLocalVariables[address - initialLocalVariablesAddress[BOOLEANO]] = value
                else:
                    self.stringLocalVariables[address - initialLocalVariablesAddress[TEXTO]] = value

            elif scope == 'temporal':
                if varType == ENTERO:
                    self.integerTemporalVariables[address - initialTemporalVariablesAddress[ENTERO]] = value
                elif varType == FLOTANTE:
                    self.floatTemporalVariables[address - initialTemporalVariablesAddress[FLOTANTE]] = value
                elif varType == BOOLEANO:
                    self.boolTemporalVariables[address - initialTemporalVariablesAddress[BOOLEANO]] = value
                else:
                    self.stringTemporalVariables[address - initialTemporalVariablesAddress[TEXTO]] = value

            elif scope == 'pointers':
                if saveAddress == True:
                    self.pointers[address - initialPointerAddress] = value
                else:
                    self.saveValue(self.pointers[address - initialPointerAddress], value)

    def getScope(self, address):
        if address >= initialGlobalAddress[ENTERO] and address < initialLocalVariablesAddress[ENTERO]:
            return 'global'
        elif address >= initialLocalVariablesAddress[ENTERO] and address < initialTemporalVariablesAddress[ENTERO]:
            return 'local'
        elif address >= initialTemporalVariablesAddress[ENTERO] and address < initialConstantsAddress[ENTERO]:
            return 'temporal'
        elif address >= initialConstantsAddress[ENTERO] and address < initialPointerAddress:
            return 'constants'
        else:
            return 'pointers'

    def getVarType(self, address, scope):
        if scope == 'global':
            if address >= initialGlobalAddress[ENTERO] and address < initialGlobalAddress[FLOTANTE]:
                return ENTERO
            elif address >= initialGlobalAddress[FLOTANTE] and address < initialGlobalAddress[BOOLEANO]:
                return FLOTANTE
            elif address >= initialGlobalAddress[BOOLEANO] and address < initialGlobalAddress[TEXTO]:
                return BOOLEANO
            else:
                return TEXTO

        elif scope == 'local':
            if address >= initialLocalVariablesAddress[ENTERO] and address < initialLocalVariablesAddress[FLOTANTE]:
                return ENTERO
            elif address >= initialLocalVariablesAddress[FLOTANTE] and address < initialLocalVariablesAddress[BOOLEANO]:
                return FLOTANTE
            elif address >= initialLocalVariablesAddress[BOOLEANO] and address < initialLocalVariablesAddress[TEXTO]:
                return BOOLEANO
            else:
                return TEXTO

        elif scope == 'temporal':
            if address >= initialTemporalVariablesAddress[ENTERO] and address < initialTemporalVariablesAddress[FLOTANTE]:
                return ENTERO
            elif address >= initialTemporalVariablesAddress[FLOTANTE] and address < initialTemporalVariablesAddress[BOOLEANO]:
                return FLOTANTE
            elif address >= initialTemporalVariablesAddress[BOOLEANO] and address < initialTemporalVariablesAddress[TEXTO]:
                return BOOLEANO
            else:
                return TEXTO

        elif scope == 'constants':
            if address >= initialConstantsAddress[ENTERO] and address < initialConstantsAddress[FLOTANTE]:
                return ENTERO
            elif address >= initialConstantsAddress[FLOTANTE] and address < initialConstantsAddress[BOOLEANO]:
                return FLOTANTE
            elif address >= initialConstantsAddress[BOOLEANO] and address < initialConstantsAddress[TEXTO]:
                return BOOLEANO
            else:
                return TEXTO

    def printMemory(self):
        # Global addresses
        print('GLOBAL ADDRESSESS')
        print(self.integerGlobalVariables)
        print(self.floatGlobalVariables)
        print(self.boolGlobalVariables)
        print(self.stringGlobalVariables)

        # Local addresses
        print('LOCAL ADDRESSESS')
        print(self.integerLocalVariables)
        print(self.floatLocalVariables)
        print(self.boolLocalVariables)
        print(self.stringLocalVariables)

        # Temporal addresses
        print('TEMPORAL ADDRESSESS')
        print(self.integerTemporalVariables)
        print(self.floatTemporalVariables)
        print(self.boolTemporalVariables)
        print(self.stringTemporalVariables)

        print('POINTERS')
        print(self.pointers)
